# import packages
import pandas as pd
import pickle as pkl
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

# get the datas and slice it at the appropriate places (days of note)
EXTRA = "_quiet_day1.pkl"
FUTURES_MBO = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\futures_orders" + EXTRA).iloc[:3289645]
FUTURES_TRD = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\futures_trades" + EXTRA).iloc[:237881]
OPTIONS_MBO = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\options_orders" + EXTRA)
OPTIONS_TRD = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\options_trades" + EXTRA)
WORKING_DELTAS = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\delta_rep_options" + EXTRA)

# function to get the index of the last timestamp before the specified time
def last_before_time(data: pd.DataFrame, time: pd.Timestamp):
    for i in range(len(data) - 1, -1, -1):
        if i == 0 and data["time"].iloc[i] > time:
            return 0
        if data["time"].iloc[i] <= time:
            return i

# function to get the index of the first timestamp after the specified time
def first_after_time(data: pd.DataFrame, time: pd.Timestamp):
    for i in range(len(data)):
        if i == len(data) - 1 and data["time"].iloc[i] < time:
            return len(data) - 1
        if data["time"].iloc[i] >= time:
            return i

# function to get the verified trades and get all the other trades from the options in the delta data
# in the exceptions list
def main():
    # get the necessary data into a dictionary form skeleton
    master_queue_dict = {}
    exceptions = {"Time": [],
                "Option": [],
                "Delta": [],
                "Security ID": [],
                "Trade Price": [],
                "Trade Quantity": [],
                "Manual Value": []}

    # go through the deltas of note
    for col in WORKING_DELTAS.columns:
        if "delta" not in col:
            continue
        append_dict = {
            "option": [],
            "time": [],
            "bid px": [],
            "bid qty": [],
            "bid orders": [],
            "ask px": [],
            "ask qty": [],
            "ask orders": [],
            "trade px": [],
            "trade qty": [],
            "queue val": []
        }
        print(col)
        for row in range(len(WORKING_DELTAS[col])):
            option = WORKING_DELTAS[col].iloc[row]
            # check if there is no trade data for the option
            if OPTIONS_TRD[option] is None:
                continue
            # get the start and end indexes within each trade data for the time intervals
            trd_st = first_after_time(OPTIONS_TRD[option], WORKING_DELTAS["time"].iloc[row])
            if(row != len(WORKING_DELTAS) - 1 and WORKING_DELTAS["time"].iloc[row + 1] - WORKING_DELTAS["time"].iloc[row] >= pd.Timedelta(days=2/3)):
                trd_en = last_before_time(OPTIONS_TRD[option], pd.Timestamp(str(WORKING_DELTAS["time"].iloc[row]).split()[0] +  " 16:00:00-05:00")) + 1 if last_before_time(OPTIONS_TRD[option], pd.Timestamp(str(WORKING_DELTAS["time"].iloc[row]).split()[0] +  " 16:00:00-05:00")) != 0 else last_before_time(OPTIONS_TRD[option], pd.Timestamp(str(WORKING_DELTAS["time"].iloc[row]).split()[0] +  " 16:00:00-05:00"))
            else:
                trd_en = first_after_time(OPTIONS_TRD[option], WORKING_DELTAS["time"].iloc[row + 1] if row != len(WORKING_DELTAS) - 1 else pd.Timestamp("2022-08-16 16:00:00-05:00"))
            # go through each of the trade data for each option and check to see if the trade can be verified
            for trd in range(len(OPTIONS_TRD[option].iloc[trd_st: trd_en])):
                prev_row = OPTIONS_MBO[option].iloc[last_before_time(OPTIONS_MBO[option], OPTIONS_TRD[option]["time"].iloc[trd_st + trd])]
                next_row = OPTIONS_MBO[option].iloc[first_after_time(OPTIONS_MBO[option], OPTIONS_TRD[option]["time"].iloc[trd_st + trd])]
                # first check if the times of prev and next rows are different since if the times are the same, this can cause an error
                if prev_row["time"] != OPTIONS_TRD[option]["time"].iloc[trd_st + trd] and next_row["time"] != OPTIONS_TRD[option]["time"].iloc[trd_st + trd]:
                    # if the bid price of the previous row equals the trade price
                    if prev_row["bid_px"] == OPTIONS_TRD[option]["px"].iloc[trd_st + trd]:
                        # check to see if the quantities make logical sense
                        if (prev_row["bid_qty"] - OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] == next_row["bid_qty"] and prev_row["bid_px"] == next_row["bid_px"]) or (prev_row["bid_qty"] == OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] and next_row["bid_px"] < prev_row["bid_px"]):
                            append_dict["option"].append(option)
                            append_dict["time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            append_dict["bid px"].append(prev_row["bid_px"])
                            append_dict["bid qty"].append(prev_row["bid_qty"])
                            append_dict["bid orders"].append(prev_row["bid_orders"])
                            append_dict["ask px"].append(prev_row["ask_px"])
                            append_dict["ask qty"].append(prev_row["ask_qty"])
                            append_dict["ask orders"].append(prev_row["ask_orders"])
                            append_dict["trade px"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            append_dict["trade qty"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                            append_dict["queue val"].append(append_dict["trade qty"][-1] / append_dict["bid qty"][-1])
                        # or else add it to the exceptions list
                        else:
                            exceptions["Time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            exceptions["Option"].append(option)
                            exceptions["Delta"].append(col)
                            exceptions["Security ID"].append(OPTIONS_TRD[option]["security_id"].iloc[trd_st + trd])
                            exceptions["Trade Price"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            exceptions["Trade Quantity"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                    # if the ask price of the previous row equals the trade price
                    elif prev_row["ask_px"] == OPTIONS_TRD[option]["px"].iloc[trd_st + trd]:
                        # check to see if the quantities make logical sense
                        if (prev_row["ask_qty"] - OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] == next_row["ask_qty"] and prev_row["ask_px"] == next_row["ask_px"]) or (prev_row["ask_qty"] == OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] and next_row["ask_px"] > prev_row["ask_px"]):
                            append_dict["option"].append(option)
                            append_dict["time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            append_dict["bid px"].append(prev_row["bid_px"])
                            append_dict["bid qty"].append(prev_row["bid_qty"])
                            append_dict["bid orders"].append(prev_row["bid_orders"])
                            append_dict["ask px"].append(prev_row["ask_px"])
                            append_dict["ask qty"].append(prev_row["ask_qty"])
                            append_dict["ask orders"].append(prev_row["ask_orders"])
                            append_dict["trade px"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            append_dict["trade qty"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                            append_dict["queue val"].append(append_dict["trade qty"][-1] / append_dict["ask qty"][-1])
                        # or else add it to the exceptions list
                        else:
                            exceptions["Time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            exceptions["Option"].append(option)
                            exceptions["Delta"].append(col)
                            exceptions["Security ID"].append(OPTIONS_TRD[option]["security_id"].iloc[trd_st + trd])
                            exceptions["Trade Price"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            exceptions["Trade Quantity"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                    # if the trade price does not equal either the previous ask or bid price, add it to the exceptions list
                    else:
                        exceptions["Time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                        exceptions["Option"].append(option)
                        exceptions["Delta"].append(col)
                        exceptions["Security ID"].append(OPTIONS_TRD[option]["security_id"].iloc[trd_st + trd])
                        exceptions["Trade Price"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                        exceptions["Trade Quantity"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                # if the prev_row and next_row times are equal
                elif prev_row["time"] == next_row["time"]:
                    prev_prev_row = OPTIONS_MBO[option].iloc[last_before_time(OPTIONS_MBO[option], OPTIONS_TRD[option]["time"].iloc[trd_st + trd]) - 1]
                    next_next_row = OPTIONS_MBO[option].iloc[first_after_time(OPTIONS_MBO[option], OPTIONS_TRD[option]["time"].iloc[trd_st + trd]) + 1]
                    # check if the previous row's bid prices match the trade price
                    if prev_prev_row["bid_px"] == OPTIONS_TRD[option]["px"].iloc[trd_st + trd]:
                        # and the bid quantities make sense
                        if (prev_prev_row["bid_qty"] - OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] == prev_row["bid_qty"] and prev_prev_row["bid_px"] == prev_row["bid_px"])  or (prev_prev_row["bid_qty"] == OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] and prev_row["bid_px"] < prev_prev_row["bid_px"]):
                            append_dict["option"].append(option)
                            append_dict["time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            append_dict["bid px"].append(prev_prev_row["bid_px"])
                            append_dict["bid qty"].append(prev_prev_row["bid_qty"])
                            append_dict["bid orders"].append(prev_prev_row["bid_orders"])
                            append_dict["ask px"].append(prev_prev_row["ask_px"])
                            append_dict["ask qty"].append(prev_prev_row["ask_qty"])
                            append_dict["ask orders"].append(prev_prev_row["ask_orders"])
                            append_dict["trade px"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            append_dict["trade qty"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                            append_dict["queue val"].append(append_dict["trade qty"][-1] / append_dict["bid qty"][-1])
                        # else add it to the exceptions list
                        else:
                            exceptions["Time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            exceptions["Option"].append(option)
                            exceptions["Delta"].append(col)
                            exceptions["Security ID"].append(OPTIONS_TRD[option]["security_id"].iloc[trd_st + trd])
                            exceptions["Trade Price"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            exceptions["Trade Quantity"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                    # check if the previous row's ask prices match the trade price
                    elif prev_prev_row["ask_px"] == OPTIONS_TRD[option]["px"].iloc[trd_st + trd]:
                        # and the ask quantities make sense
                        if (prev_prev_row["ask_qty"] - OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] == prev_row["ask_qty"] and prev_prev_row["ask_px"] == prev_row["ask_px"])  or (prev_prev_row["ask_qty"] == OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] and prev_row["ask_px"] > prev_prev_row["ask_px"]):
                            append_dict["option"].append(option)
                            append_dict["time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            append_dict["bid px"].append(prev_prev_row["bid_px"])
                            append_dict["bid qty"].append(prev_prev_row["bid_qty"])
                            append_dict["bid orders"].append(prev_prev_row["bid_orders"])
                            append_dict["ask px"].append(prev_prev_row["ask_px"])
                            append_dict["ask qty"].append(prev_prev_row["ask_qty"])
                            append_dict["ask orders"].append(prev_prev_row["ask_orders"])
                            append_dict["trade px"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            append_dict["trade qty"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                            append_dict["queue val"].append(append_dict["trade qty"][-1] / append_dict["ask qty"][-1])
                        # else add it to the exceptions list
                        else:
                            exceptions["Time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            exceptions["Option"].append(option)
                            exceptions["Delta"].append(col)
                            exceptions["Security ID"].append(OPTIONS_TRD[option]["security_id"].iloc[trd_st + trd])
                            exceptions["Trade Price"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            exceptions["Trade Quantity"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                    # check if the next row's bid prices match the trade price
                    elif next_row["bid_px"] == OPTIONS_TRD["px"].iloc[trd_st + trd]:
                        # and the bid quantities make sense
                        if (next_row["bid_qty"] - OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] == next_next_row["bid_qty"] and next_row["bid_px"] == next_next_row["bid_px"])  or (next_row["bid_qty"] == OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] and next_row["bid_px"] > next_next_row["bid_px"]):
                            append_dict["option"].append(option)
                            append_dict["time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            append_dict["bid px"].append(next_row["bid_px"])
                            append_dict["bid qty"].append(next_row["bid_qty"])
                            append_dict["bid orders"].append(next_row["bid_orders"])
                            append_dict["ask px"].append(next_row["ask_px"])
                            append_dict["ask qty"].append(next_row["ask_qty"])
                            append_dict["ask orders"].append(next_row["ask_orders"])
                            append_dict["trade px"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            append_dict["trade qty"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                            append_dict["queue val"].append(append_dict["trade qty"][-1] / append_dict["bid qty"][-1])
                        # else add it to the exceptions list
                        else:
                            exceptions["Time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            exceptions["Option"].append(option)
                            exceptions["Delta"].append(col)
                            exceptions["Security ID"].append(OPTIONS_TRD[option]["security_id"].iloc[trd_st + trd])
                            exceptions["Trade Price"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            exceptions["Trade Quantity"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                    # check if the next row's ask prices match the trade price
                    elif next_row["ask_px"] == OPTIONS_TRD[option]["px"].iloc[trd_st + trd]:
                        # and the ask quantities make sense
                        if (next_row["ask_qty"] - OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] == next_next_row["ask_qty"] and next_row["ask_px"] == next_next_row["ask_px"])  or (next_row["ask_qty"] == OPTIONS_TRD[option]["qty"].iloc[trd_st + trd] and next_row["ask_px"] < next_next_row["ask_px"]):
                            append_dict["option"].append(option)
                            append_dict["time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            append_dict["bid px"].append(next_row["bid_px"])
                            append_dict["bid qty"].append(next_row["bid_qty"])
                            append_dict["bid orders"].append(next_row["bid_orders"])
                            append_dict["ask px"].append(next_row["ask_px"])
                            append_dict["ask qty"].append(next_row["ask_qty"])
                            append_dict["ask orders"].append(next_row["ask_orders"])
                            append_dict["trade px"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            append_dict["trade qty"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                            append_dict["queue val"].append(append_dict["trade qty"][-1] / append_dict["ask qty"][-1])
                        # else add it to the exceptions list
                        else:
                            exceptions["Time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                            exceptions["Option"].append(option)
                            exceptions["Delta"].append(col)
                            exceptions["Security ID"].append(OPTIONS_TRD[option]["security_id"].iloc[trd_st + trd])
                            exceptions["Trade Price"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                            exceptions["Trade Quantity"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                    else:
                        exceptions["Time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                        exceptions["Option"].append(option)
                        exceptions["Delta"].append(col)
                        exceptions["Security ID"].append(OPTIONS_TRD[option]["security_id"].iloc[trd_st + trd])
                        exceptions["Trade Price"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                        exceptions["Trade Quantity"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])
                else:
                    exceptions["Time"].append(OPTIONS_TRD[option]["time"].iloc[trd_st + trd])
                    exceptions["Option"].append(option)
                    exceptions["Delta"].append(col)
                    exceptions["Security ID"].append(OPTIONS_TRD[option]["security_id"].iloc[trd_st + trd])
                    exceptions["Trade Price"].append(OPTIONS_TRD[option]["px"].iloc[trd_st + trd])
                    exceptions["Trade Quantity"].append(OPTIONS_TRD[option]["qty"].iloc[trd_st + trd])

        master_queue_dict[col] = pd.DataFrame(append_dict)

    print(master_queue_dict)
    exceptions["Manual Value"] = [None for _ in range(len(exceptions["Time"]))]
    print(pd.DataFrame(exceptions))

    with open('queue_master_data' + EXTRA, 'wb') as f:
        pkl.dump(master_queue_dict, f)

    with open('exceptions' + EXTRA, 'wb') as f:
        pkl.dump(pd.DataFrame(exceptions), f)

# function to change and revise some of the trades that were in the exceptions list
def modify():
    EXCEPTIONS = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\exceptions" + EXTRA)
    WORKING_QUEUES = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\queue_master_data" + EXTRA)
    if EXTRA == "_quiet_day.pkl":
        revisions = [None, None, None, None, None, None, None, None, None, None, None, 0.02, 1.0, 0.8, 1.0, 
        None, 0.24, 1.0/42.0, 1.0/19.0, 1.0/11.0, 1.0/9.0, 0.8, 1.0, 1.0/26.0, 3.0/7.0, 1.0/58.0]
    elif EXTRA == "_quiet_day1.pkl":
        revisions = [None, 1.0/64.0, 1.0/22.0, 1.0, None, 0.1, None, None, None, None,
                     None, None, None, None, 1.0/65.0, 1.0/21.0, 1.0/111.0, None, None, 0.8,
                     1.0, None, None, 0.8, 1.0, 1.0/12.0, None, 1.0/16.0, 0.5, 0.16,
                     1.0/21.0, 0.02, 1.0, 0.8, 1.0, 1.0, 0.1, None, None, None,
                     None, None, None, None, None, None, None, None, None, 4.0/9.0,
                     None, None, None, None, 0.5, 2.0/45.0, 1.0, None, None, None,
                     None, None, None, 1.0/42.0, 1.0/94.0, None, None, None, None, None,
                     None, None, None, None, None, None, None, None, None, 0.02,
                     None, 1.0/246.0, None, 9.0/17.0, None, 8.0/14.0, 5.0/33.0, 0.8, 1.0, 3.0/81.0,
                     25.0/145.0, 0.16, 1.0, 5.0/56.0, 1.0/67.0, 0.24, 0.8, 1.0, 1.0/31.0, None,
                     1.0/78.0, 1.0/19.0, 1.0/11.0, 1.0/9.0, 0.5, 1.0, 1.0/9.0, 1.0/94.0, 2.0/12.0, 1.0,
                     None, None, None, None, None, None, None, None, None, None,
                     None, None, None, None, None, None, None, None, None, None,
                     1.0/8.0, None, 3.0/7.0, 1.0]
        add_none = [None for _ in range(len(revisions), len(EXCEPTIONS))]
        revisions = revisions + add_none
    else:
        revisions = [None for _ in range(len(EXCEPTIONS))]
    for i in range(len(revisions)):
        EXCEPTIONS.at[i, "Manual Value"] = revisions[i]
        if revisions[i] is not None:
            WORKING_QUEUES[EXCEPTIONS.at[i, "Delta"]].loc[len(WORKING_QUEUES[EXCEPTIONS.at[i, "Delta"]].index)] = [EXCEPTIONS.at[i, "Option"], EXCEPTIONS.at[i, "Time"], None, None, None, None, None, None, EXCEPTIONS.at[i, "Trade Price"], EXCEPTIONS.at[i, "Trade Quantity"], EXCEPTIONS.at[i, "Manual Value"]]

    with open('queue_master_data_fixed' + EXTRA, 'wb') as f:
        pkl.dump(WORKING_QUEUES, f)

# graph some quantiles of the trade percentages
def graph():
    WORKING_QUEUES = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\queue_master_data_fixed" + EXTRA)
    graph_dict = {
        "delta": [],
        "10th percentile": [],
        "20th percentile": [],
        "30th percentile": [],
        "50th percentile": [],
        "75th percentile": [],
        "Average": []
    }
    PUTS_OR_CALLS = 1
    for delta in WORKING_QUEUES:
        WORKING_QUEUES[delta] = WORKING_QUEUES[delta].loc[(WORKING_QUEUES[delta]["trade qty"] != 1) | (WORKING_QUEUES[delta]["queue val"] != 1)]
        if len(WORKING_QUEUES[delta]) >= (12 if PUTS_OR_CALLS == -1 else 15) and delta != "delta -40" and (int(delta.split()[1]) > 0 if PUTS_OR_CALLS == 1 else int(delta.split()[1]) < 0):
            #graph_dict["delta"].append(abs(int(delta.split()[1])))
            working_queue_list = np.array(WORKING_QUEUES[delta]['queue val'])
            graph_dict["delta"].append(abs(int(delta.split()[1])))
            graph_dict["10th percentile"].append(100 * np.percentile(working_queue_list, 10))
            graph_dict["20th percentile"].append(100 * np.percentile(working_queue_list, 20))
            graph_dict["30th percentile"].append(100 * np.percentile(working_queue_list, 30))
            graph_dict["50th percentile"].append(100 * np.percentile(working_queue_list, 50))
            graph_dict["75th percentile"].append(100 * np.percentile(working_queue_list, 75))
            sum = 0
            for i in range(len(WORKING_QUEUES[delta])):
                #for i in range(len(working_queue_list)):
                sum += working_queue_list[i]
            graph_dict["Average"].append(sum / len(WORKING_QUEUES[delta]))

    plt.plot(graph_dict["delta"], graph_dict["10th percentile"], 'rx', label="10th percentile")
    #plt.plot(graph_dict["delta"], savgol_filter(graph_dict["10th percentile"], 11, 2), 'r', label="10th percentile")
    #plt.plot(graph_dict["delta"], graph_dict["20th percentile"], 'gx')
    #plt.plot(graph_dict["delta"], savgol_filter(graph_dict["20th percentile"], 11, 2), 'g', label='20th percentile')
    plt.plot(graph_dict["delta"], graph_dict["30th percentile"], 'gx', label="30th percentile")
    #plt.plot(graph_dict["delta"], savgol_filter(graph_dict["30th percentile"], 11, 2), 'b', label="30th percentile")
    plt.plot(graph_dict["delta"], graph_dict["50th percentile"], 'bx', label="50th percentile")
    #plt.plot(graph_dict["delta"], savgol_filter(graph_dict["50th percentile"], 11, 2), 'k', label="50th percentile")
    #plt.plot(graph_dict["delta"], graph_dict["Average"], 'y--', alpha=0.2, label="Average Value")
    #plt.plot(graph_dict["delta"], graph_dict["75th percentile"], 'kx', label="75th percentile")
    plt.xlim([min(graph_dict["delta"]) - 1, 51])
    plt.xlabel("Deltas")
    plt.ylabel("Trade %")
    plt.title("Percentiles of " + ("Call" if PUTS_OR_CALLS == 1 else "Put") + " Options Top-Level Trade %")
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, -.1),
          ncol=3, fancybox=True, shadow=True)
    plt.grid()
    #plt.savefig('queue_values.png', bbox_inches='tight')
    plt.show()


if __name__ == "__main__":
    #main()
    #modify()
    graph()

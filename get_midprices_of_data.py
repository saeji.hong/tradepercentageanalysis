# download the necessary packages
import pandas as pd
from data_project_fast_functions import *
import pickle as pkl

# get the data that was downloaded from the hekla file
EXTRA = "_quiet_day1.pkl"
fut_mbo = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\futures_orders" + EXTRA) # TOB data for LOV2 futures
fut_trd = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\futures_trades" + EXTRA) # trades data for LOV2 futures
opt_mbo = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\options_orders" + EXTRA) # TOB data for each of the LOV2 options
opt_trd = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\options_trades" + EXTRA) # trades data for each of the LOV2 options

DAYS = ["20220815", "20220816", "20220817", "20220818", "20220819"] # days that this data covers

# add the columns necessary to calculate midprices for the futures
fut_mbo['big_spr'], _ = classify_BIG_SPR(fut_mbo, ['bid_px', 'ask_px'], 7, 3) # add a "big spread" column
fut_mbo["mid_px"] = np.array(EASIER_ESTIMATE(fut_mbo, 'bid_px', 'ask_px', 'big_spr')) # add the midprice column

# write this data into a pickle file
with open('combined_futures' + EXTRA, 'wb') as f:
    pkl.dump(fut_mbo, f)

# go through the options data and calculate the midprices and add this as a new column
# into the options dictionary
for symb, data in opt_mbo.items():
    new_data = data
    new_data['big_spr'], _ = classify_BIG_SPR(data, 'bid_px', 'ask_px', 15, 5)
    new_data["mid_px"] = np.array(EASIER_ESTIMATE(data, 'bid_px', 'ask_px', 'big_spr'))
    opt_mbo[symb] = new_data
    print(symb)

# write the dictionary to a pickle file
with open('combined_options' + EXTRA, 'wb') as f:
    pkl.dump(opt_mbo, f)

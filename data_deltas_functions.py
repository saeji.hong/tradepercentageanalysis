import pandas as pd

# get the index of the minimum value of the list
def get_min_index(int_list: list) -> int:
    a = min(int_list)
    return int_list.index(a)

# another function to get the index of the minimum value of the list
def get_min_idx(int_list: list) -> int:
    for i in range(1, len(int_list)):
        if (int_list[i] * int_list[i - 1]) < 0:
            return i if abs(int_list[i]) < abs(int_list[i - 1]) else i - 1
        if int_list[i] == 0:
            return i

# get the option price at a certain time
def get_opt_px_at_time(data: pd.DataFrame, time) -> float:
    work_data = data[['time', 'mid_px']]
    return_idx = 0
    for i in range(len(work_data)):
        if work_data['time'].iloc[i] > time:
            return_idx = max(0, i - 1)
            return work_data['mid_px'].iloc[return_idx]

# convert a pd.Timestamp to a string
def convert_timestamp_to_str(timestamp: pd.Timestamp, dec=0) -> str:
    time_str = str(timestamp)[:str(timestamp).rfind("-")]
    seconds = int(float(time_str[time_str.rfind(":") + 1: ]))
    if dec > 0:
        seconds = int(float(time_str[time_str.rfind(":") + 1: ]) * 10 ** dec) / 10 ** dec
    time_str = time_str[: time_str.rfind(":") + 1]
    time_str += str(seconds) if len(str(int(seconds))) == 2 else "0" + str(seconds)
    return_str = ""
    for i in time_str:
        if i != "-":
            return_str += i
    return return_str

# get the strike of an option
def extract_strike(symb: str) -> int:
    tick = symb.split(" ")[1][1:]
    return int(tick)

# get the index of the last row before the timestamp
def get_last_time_before(data: pd.DataFrame, timestamp: str) -> int:
    if isinstance(timestamp, str):
        timestamp = pd.Timestamp(timestamp)
    for i in range(len(data) - 1, -1, -1):
        time1 = pd.Timestamp(convert_timestamp_to_str(data["time"].iloc[i]))
        if time1 < timestamp:
            return i

# get the type of an option
def extract_type(symb: str) -> int:
    if symb.split(" ")[1][0] == "C":
        return 1
    elif symb.split(" ")[1][0] == "P":
        return -1
    else:
        raise Exception("ERROR")

# get the string form of option symbol
def revert_to_symb(symb: str, num: int) -> str:
    if num > 0:
        return symb + " C" + str(num)
    elif num < 0:
        return symb + " P" + str(-num)

# compare a string time and pd.Timestamp
def compare_timestamp_with_str(timestamp: pd.Timestamp, time2: str) -> int:
    if isinstance(time2, str):
        time1 = convert_timestamp_to_str(timestamp)
        time1 = pd.Timestamp(time1)
        time2 = pd.Timestamp(time2)
    if time1 > time2:
        return 1
    if time1 < time2:
        return -1
    return 0

# rounding function
def round_to_nearest_x(num, x=20) -> float:
    return x * round(num / x)

# get the index of the last row before the timestamp
def last_before_time(data: pd.DataFrame, time: pd.Timestamp) -> int:
    for i in range(len(data) - 1, -1, -1):
        if i == 0 and data["time"].iloc[i] > time:
            return 0
        if data["time"].iloc[i] < time:
            return i
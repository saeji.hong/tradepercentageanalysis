import pandas as pd
import numpy as np

# compare estimate with bid price and if its less than the buffered value, return False
# otherwise True
def compare_est_to_bid(estimate: float, bid_px: float, buffer=0.25) -> bool:
    if estimate < bid_px + buffer:
        return False
    return True

# compare estimate with ask price and if its greater than the buffered value, return False
# otherwise True
def compare_est_to_ask(estimate: float, ask_px: float, buffer=0.25) -> bool:
    if estimate > ask_px - buffer:
        return False
    return True

# takes a bid and ask price and gets the average of it
def simple_mid(bid: float, ask: float) -> float:    
    return (bid + ask) / 2  

# compare the estimate with the bid and ask prices; if the estimate is within a biffered amount
# from the bid or ask prices, buffer it to either bid + buffer or ask - buffer depending on
# if the estimate is too high or too low
def buffer_capped_estimate(estimate: float, lower_bound: float, upper_bound: float, buffer=0.25) -> float:
    if not compare_est_to_bid(estimate, lower_bound, buffer):
        return lower_bound + buffer
    if not compare_est_to_ask(estimate, upper_bound, buffer):
        return upper_bound - buffer
    return estimate          

# a simple estimator that takes the average of the bid and ask prices unless there is a "wide"
# spread in which case we take the previous timestamp's estimate and then buffer it so that 
# it is inside the bid/ask spread if it wasn't
def EASIER_ESTIMATE(data: pd.DataFrame, bid_col: str, ask_col: str, big_spread_col: str) -> list:
    output_estimate = []
    for i in range(len(data)):
        x_curr = float(data[bid_col].iloc[i])
        y_curr = float(data[ask_col].iloc[i])
        if data[big_spread_col].iloc[i] == 0:
            output_estimate.append(simple_mid(x_curr, y_curr))
        else:
            output_estimate.append(buffer_capped_estimate(output_estimate[-1], x_curr, y_curr, 0.5))
    return output_estimate

# a function that looks at the bid/ask spread as well as the change in the bids and ask prices
# and classifies the spread as either being "too wide" (1, 2, 3) or not (0)
def classify_BIG_SPR(data: pd.DataFrame, bid_col: str, ask_col: str, MAX_SPREAD: float, BUFFER: float, symb="") -> tuple:
    # check if the data is a DataFrame
    
    if not isinstance(data, pd.DataFrame) or not isinstance(data, pd.core.frame.DataFrame):
        print("INVALID INPUT")
        if data is None:
            print(symb)
        raise TypeError("Please input a dataframe")

    output_list = []
    big_spr_eas = []

    # X will be the data for one column
    # Y will be the data for the other
    X = data[bid_col]
    Y = data[ask_col]
    prev_big = False

    for i in range(len(X)):
        if i == 0:
            output_list.append(0) # for the first value, we will always classify the spread as 0
            big_spr_eas.append(0)
            continue
        if not prev_big and float(Y.iloc[i]) - float(X.iloc[i]) < MAX_SPREAD:
            output_list.append(0)
            big_spr_eas.append(0)
            continue

        prev_X = float(X.iloc[i - 1])
        prev_Y = float(Y.iloc[i - 1])
        curr_X = float(X.iloc[i])
        curr_Y = float(Y.iloc[i])
        big_spr_eas.append(1)
        if abs(curr_X - prev_X) >= BUFFER:
            if abs(curr_Y - prev_Y) >= BUFFER:
                output_list.append(3)
                prev_big = True
            else:
                output_list.append(1)
                prev_big = True
        elif abs(curr_Y - prev_Y) >= BUFFER:
            output_list.append(2)
            prev_big = True
        else:
            prev = output_list[-1]
            output_list.append(prev) # not a drastic change that caused a widening of the spread/market but is a special case
    
        if float(Y.iloc[i]) - float(X.iloc[i]) < MAX_SPREAD:
            prev_big = False

    return output_list, big_spr_eas

# import necessary packages
from site_packages.modelzoo.model_utils.volatility_models.black76 import Black76Calculator
from modelzoo.utils.put_call import PutCall
import pandas as pd
import matplotlib.pyplot as plt
from hekla.py.examples.ok.util.vt_utils import VTUtil
import numpy as np
import random
import pickle as pkl
from data_deltas_functions import *
import matplotlib as mpl

# get the necessary data
TYPE = "LONG QUIET"
extra = "_quiet_day1.pkl"
WORKING_FUT_DATA = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\combined_futures" + extra)
# slice the data in the days of interest
WORKING_FUT_DATA = WORKING_FUT_DATA.iloc[766343: 3289645] #1208029, 2162231
WORKING_OPT_DATA = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\combined_options" + extra)
WORKING_DELTAS = pd.read_pickle(r"C:\Users\TEMP\Documents\Python Scripts\delta_master_table" + extra)
CONTRACT_EXP_DATE = "20220915 16:00:00"
DAY = ["20220816", "20220817", "20220818"]
SYMBOL = "LOV2"
r = 0.0277

# order the options data from highest strike put to highest strike call
to_order = [extract_strike(sym) * extract_type(sym) for sym in WORKING_OPT_DATA.keys()]
to_order.sort()
to_order = [revert_to_symb(SYMBOL, x) for x in to_order]
WORKING_OPT_DATA = {k : WORKING_OPT_DATA[k] for k in to_order}

#----------------------------------------------------------SMILE GRAPH---------------------------------------------------------
# get the volatility smile at time st
def get_vol_smiles(st: str):
    F = WORKING_FUT_DATA["mid_px"].iloc[get_last_time_before(WORKING_FUT_DATA, st)]
    r = 0.0242
    X1 = []
    Y1 = []
    X2 = []
    Y2 = []
    # go through the options data and find all the relevant numbers to get the volatility at 
    # each option at this time
    for symb, data in WORKING_OPT_DATA.items():
        K = extract_strike(symb)
        t = VTUtil(st, CONTRACT_EXP_DATE)
        t = t.find_t()
        px = data["mid_px"].iloc[get_last_time_before(data, st)]
        VOL = Black76Calculator.solve_vol(F, K, px, t, r, PutCall.CALL if extract_type(symb) == 1 else PutCall.PUT)
        if extract_type(symb) == 1:
            X1.append(K)
            Y1.append(VOL)
        elif extract_type(symb) == -1:
            X2.append(K)
            Y2.append(VOL)
    
    # graph the call and put volatility smiles
    plt.plot(X1, Y1, 'bx', label="CALLS")
    plt.plot(X2, Y2, 'rx', label="PUTS")
    plt.legend()
    plt.grid()
    plt.xlabel("Strikes")
    plt.ylabel("Volatility")
    plt.title("Volatility Smiles")
    plt.savefig("Volatility_Smile.png")
    plt.show()

#----------------------------------------------------------DELTA GRAPH---------------------------------------------------------
# get the delta curves for both calls and puts at time st
def get_deltas(st: str):
    F = WORKING_FUT_DATA["mid_px"].iloc[get_last_time_before(WORKING_FUT_DATA, st)]
    X1 = []
    Y1 = []
    X2 = []
    Y2 = []
    # go through the options data and extract the deltas at each option
    # at time st
    for symb, data in WORKING_OPT_DATA.items():
        K = extract_strike(symb)
        t = VTUtil(st, CONTRACT_EXP_DATE)
        t = t.find_t()
        px = data["mid_px"].iloc[get_last_time_before(data, st)]
        VOL = Black76Calculator.solve_vol(F, K, px, t, r, PutCall.CALL if extract_type(symb) == 1 else PutCall.PUT)
        DELTA = Black76Calculator.delta(F, K, VOL, t, r, PutCall.CALL if extract_type(symb) == 1 else PutCall.PUT)
        if extract_type(symb) == 1:
            X1.append(K)
            Y1.append(DELTA)
        elif extract_type(symb) == -1:
            X2.append(K)
            Y2.append(DELTA)

    # graph the delta curves
    plt.plot(X1, Y1, 'bx', label="CALLS")
    plt.plot(X2, Y2, 'rx', label="PUTS")
    plt.legend()
    plt.grid()
    plt.xlabel('Strikes')
    plt.ylabel('Delta')
    plt.title("Delta Graphs")
    plt.savefig("Delta_Graphs.png")
    plt.show()

#----------------------------------------------------------DELTA TABLE---------------------------------------------------------
# get the deltas of each option at a certain time, which will include any intervals of time
# where the futures price moved by 20 ticks or if 20 min have passed without a 20 tick change 
# in the futures price
def get_del_tb():

    # Dataframe that will contain all the data for all the days specified
    master_master_day = None
    
    for td in DAY:
        # splice the data to get the times between 9:30 am and 4:00 pm (regular trading hours)
        idx1 = 0
        idx2 = 0
        for i in range(len(WORKING_FUT_DATA)):
            if compare_timestamp_with_str(WORKING_FUT_DATA["time"].iloc[i], td + " 09:29:59") == 1:
                idx1 = i
                break
        for i in range(len(WORKING_FUT_DATA) - 1, -1, -1):
            if compare_timestamp_with_str(WORKING_FUT_DATA["time"].iloc[i], td + " 16:00:00") == -1:
                idx2 = i
                break

        # create the dictionary that will store all of the values
        master_dict = {"time": [],
                    "fut_px": []}
        for symb, data in WORKING_OPT_DATA.items():
            if extract_strike(symb) * extract_type(symb) >= -11000 and extract_strike(symb) * extract_type(symb) <= 15000:
                master_dict[symb] = []

        '''
            Note: There can be some error in this due to the fact that some options may not have any mbo tob data at all in the early or 
            later stages of the day which can cause erroneous estimates of midprices at certain times for each option
        '''
        # go through each of the rows in the normal trading hours
        for i in range(idx1, idx2 + 1, 1):
            # see if the futures price is close to a rounded 20 tick value and that it's different from the previous
            if abs(round_to_nearest_x(WORKING_FUT_DATA["mid_px"].iloc[i]) - WORKING_FUT_DATA["mid_px"].iloc[i]) <= 1 and (len(master_dict["fut_px"]) == 0 or round_to_nearest_x(WORKING_FUT_DATA["mid_px"].iloc[i]) != master_dict["fut_px"][-1]):
                F = WORKING_FUT_DATA["mid_px"].iloc[i]
                roundf = round_to_nearest_x(F)
                time = WORKING_FUT_DATA["time"].iloc[i]
                master_dict["time"].append(time)
                master_dict["fut_px"].append(roundf)
                print("Futures Price @ " + convert_timestamp_to_str(time, 3) + ": " + str(roundf))
            # check if more than 20 minutes have passed from the last previous recorded time
            elif len(master_dict["time"]) == 0 or (WORKING_FUT_DATA["time"].iloc[i] - master_dict["time"][-1] > pd.Timestamp(td + " 10:20:00") - pd.Timestamp(td + " 10:00:00")):
                F = WORKING_FUT_DATA["mid_px"].iloc[i]
                time = WORKING_FUT_DATA["time"].iloc[i]
                master_dict["time"].append(time)
                master_dict["fut_px"].append(F)
                print("Futures Price @ " + convert_timestamp_to_str(time, 3) + ": " + str(F))
            else:
                continue

            # go through the options and get the deltas at each
            for symb, data in WORKING_OPT_DATA.items():
                # since we only care about deltas less than around 50, we will take some options out of the delta recordings
                if extract_strike(symb) * extract_type(symb) < -11000 or extract_strike(symb) * extract_type(symb) > 15000:
                    continue
                index = last_before_time(data, time)
                if data["time"].iloc[index] > time and index != 0:
                    raise Exception("ERROR IN LAST_BEFORE_TIME FUNCTION")
                if index + 1 < len(data) and data["time"].iloc[index + 1] < time:
                    raise Exception("ERROR IN LAST_BEFORE_TIME FUNCTION")
                K = extract_strike(symb)
                t = VTUtil(convert_timestamp_to_str(time), CONTRACT_EXP_DATE)
                t = t.find_t()
                px = data["mid_px"].iloc[index]
                VOL = Black76Calculator.solve_vol(F, K, px, t, r, PutCall.CALL if extract_type(symb) == 1 else PutCall.PUT)
                DELTA = Black76Calculator.delta(F, K, VOL, t, r, PutCall.CALL if extract_type(symb) == 1 else PutCall.PUT)
                master_dict[symb].append(round(DELTA * 10000000) / 100000)
                if K % 1000 == 0:
                    print(symb + " Delta @ " + convert_timestamp_to_str(time, 3) + ": " + str(master_dict[symb][-1]))

            print()
            print(pd.DataFrame.from_dict(master_dict))
            print()
            print()
            print()
        # add the day's data to the master data
        master_master_day = pd.concat([master_master_day, pd.DataFrame.from_dict(master_dict)]).reset_index(drop=True)
        
        # optional graph to plot the deltas
        '''
        if random.randint(0, 9) != 0:
            X1 = []
            Y1 = []
            X2 = []
            Y2 = []
            deltas = pd.DataFrame.from_dict(master_dict).iloc[[-1]]
            for col in deltas.columns:
                if col == "time" or col == "fut_px":
                    continue
                if extract_type(col) == 1:
                    X1.append(extract_strike(col))
                    Y1.append(deltas[col].iloc[0])
                elif extract_type(col) == -1:
                    X2.append(extract_strike(col))
                    Y2.append(deltas[col].iloc[0])
            plt.plot(X1, Y1, 'bx', label="CALLS")
            plt.plot(X2, Y2, 'rx', label="PUTS")
            plt.grid()
            plt.show()'''

    with open('delta_master_table' + extra, 'wb') as f:
        pkl.dump(pd.DataFrame.from_dict(master_master_day), f)

#---------------------------------------------------------DELTA CLEANUP--------------------------------------------------------
# read the deltas table and find the representative option for each of the delta values of note
# which will be defined by the option that has a delta value closest to the value of the delta 
# of note
def get_int_opt_deltas():

    # deltas of note
    int_deltas = [-50, -40, -30, -20, -15, -10, -5, -3, -2, -1, 1, 2, 3, 5, 10, 15, 20, 30, 40, 50]
    delta_of_note_dict = {
        "time": [],
        "fut_px": []
    }
    for i in int_deltas:
        delta_of_note_dict["delta " + str(i)] = []
    DELTAS = WORKING_DELTAS.iloc[:, WORKING_DELTAS.columns != "time"]
    DELTAS = DELTAS.iloc[:, DELTAS.columns != "fut_px"]
    list_col = DELTAS.columns

    # go through each row of the deltas table and find the representative option
    for row in range(len(DELTAS)):
        working_row = np.array(DELTAS.iloc[[row]]).flatten()
        delta_of_note_dict["time"].append(WORKING_DELTAS["time"].iloc[row])
        delta_of_note_dict["fut_px"].append(WORKING_DELTAS["fut_px"].iloc[row])
        for d in int_deltas:
            if d < 0:
                work_list = [x - d for x in working_row]
            else:
                work_list = [working_row[i] - d for i in range(len(working_row) - 1, -1, -1)]
            index = get_min_idx(work_list) if d < 0 else len(work_list) - 1 - get_min_idx(work_list)
            # this is for buffering the lower deltas since the lower deltas should not be changing
            # representative options too much and due to the limitations of the volatility not hav-
            # ing the buffer would mean a lot of change in this
            if len(delta_of_note_dict["delta " + str(d)]) != 0 and (abs(d) == 1 or abs(d) == 2):
                prev = list(list_col).index(delta_of_note_dict["delta " + str(d)][-1])
                if abs(work_list[index] - work_list[prev]) <= 0.005 * abs(work_list[prev]):
                    delta_of_note_dict["delta " + str(d)].append(list_col[prev])
                    continue
            delta_of_note_dict["delta " + str(d)].append(list_col[index])

    # write to a pickle file
    delta_of_note_dict = pd.DataFrame(delta_of_note_dict)
    with open('delta_rep_options' + extra, 'wb') as f:
        pkl.dump(delta_of_note_dict, f)

    def colorFader(c1,c2,mix=0):
        c1=np.array(mpl.colors.to_rgb(c1))
        c2=np.array(mpl.colors.to_rgb(c2))
        return mpl.colors.to_hex((1-mix)*c1 + mix*c2)

    # graph the neg and pos deltas and the representative option strikes
    c1='red'
    c2='blue'
    n=len(list(delta_of_note_dict.columns)[2: 2 + int(len(int_deltas) / 2)])
    count = 0
    for i in list(delta_of_note_dict.columns)[2: 2 + int(len(int_deltas) / 2)]:
        working_list = [extract_strike(x) for x in delta_of_note_dict[i]]
        plt.plot(range(len(working_list)), working_list, color=colorFader(c1,c2,count/n), label=i)
        #if count == 3:
        #    plt.fill_between(range(len(working_list)), working_list, 16000,  color="red", alpha=0.2)
        count += 1
    #plt.title("Changing " + SYMBOL + " Put Option Strikes per Delta on " + str(DAY))
    plt.title("Rolling LOV2 Put Option Strikes on 08/16-18/2022")
    plt.ylim([4000, 9300])
    plt.xlabel("Time Intervals")
    plt.ylabel("Representative Option Strikes")
    plt.grid()
    plt.plot(range(len(working_list)), np.array(WORKING_DELTAS["fut_px"]).flatten(), 'k-', label="fut_px")
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 0.22),
          ncol=4, fancybox=True, shadow=True)
    plt.savefig("strike_put_change_deltas.png")
    plt.show()

    c1='blue'
    c2='red'
    n=len(list(delta_of_note_dict.columns)[2 + int(len(int_deltas) / 2): ])
    count = 0
    for i in list(delta_of_note_dict.columns)[2 + int(len(int_deltas) / 2): ]:
        working_list = [extract_strike(x) for x in np.array(delta_of_note_dict[i])]
        plt.plot(range(len(working_list)), working_list, color=colorFader(c1,c2,count/n), label=i)
        #if count == int(len(int_deltas) / 2) - 4:
        #    plt.fill_between(range(len(working_list)), working_list, 2000,  color="red", alpha=0.2)
        count += 1
    #plt.title("Changing "+ SYMBOL + " Call Option Strikes per Delta on " + str(DAY))
    plt.title("Rolling LOV2 Call Option Strikes on 08/16-18/2022")
    plt.ylim([7000, 13700])
    plt.xlabel("Time Intervals")
    plt.ylabel("Representative Option Strikes")
    plt.grid()
    plt.plot(range(len(working_list)), np.array(WORKING_DELTAS["fut_px"]).flatten(), 'k-', label="fut_px")
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 0.22),
          ncol=4, fancybox=True, shadow=True)
    plt.savefig("strike_call_change_deltas.png")
    plt.show()

        
if __name__ == "__main__":
    #get_vol_smiles("20220705 12:13:00")
    #get_deltas("20220705 12:13:00")
    #get_del_tb()
    get_int_opt_deltas()
